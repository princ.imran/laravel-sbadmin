<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Homeroute extends Controller
{
    public function wel(){
    	return view('front/index');
    }

    public function admin(){
    	return view('admin/index');
    }

    public function create(){
    	return view('admin/student/create');
    }

    public function index($name,$id){
    	return view('admin/student/index', compact('id','name'));
    }
}
